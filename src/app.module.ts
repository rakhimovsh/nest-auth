import { Module } from '@nestjs/common';
import { UserModule } from './modules/user/user.module';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './modules/auth/auth.module';
import { AppController } from './app.controller';

@Module({
  imports: [UserModule, MongooseModule.forRoot('mongodb://localhost/auth'), AuthModule],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
