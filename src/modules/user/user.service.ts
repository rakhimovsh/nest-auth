import { Injectable } from '@nestjs/common';
import { InjectModel } from "@nestjs/mongoose";
import { User, UserDocument } from "./user.schema";
import { Model } from "mongoose";
@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {
  }

  async getAllUsers(): Promise<User[]> {
    return await this.userModel.find({}).lean()
  }

  async findByUsername(username: string): Promise<User | null>{
    return await this.userModel.findOne({username}).lean()
  }
  async findById(id: string): Promise<User | null>{
    return await this.userModel.findById(id).lean()
  }
}
