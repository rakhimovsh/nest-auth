import { Prop, SchemaFactory, Schema } from "@nestjs/mongoose";
import { HydratedDocument, ObjectId } from "mongoose";

export type UserDocument = HydratedDocument<User>;
@Schema({timestamps: true, versionKey: false})
export class User{
  _id: ObjectId;


  @Prop()
  username: string

  @Prop()
  password: string

  @Prop()
  fullName: string

  @Prop()
  roles: string[]

}

export const UserSchema = SchemaFactory.createForClass(User);