import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { jwtSecret } from "../constants";
import { UserPayload } from "../auth.interface";
import { UserService } from "../../user/user.service";



@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy){
  constructor(private userService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtSecret
    });
  }

  async validate(payload: UserPayload){
    const user = await this.userService.findById(payload.sub)
    delete user.password
    return user
  }
}