import { Module } from '@nestjs/common';
import {PassportModule} from "@nestjs/passport";
import { AuthService } from './auth.service';
import { UserModule } from "../user/user.module";
import {LocalStrategy} from "./strategies/local.strategy";
import { JwtModule } from "@nestjs/jwt";
import { jwtSecret } from "./constants";
import { JwtStrategy } from "./strategies/jwt.strategy";

@Module({
  imports: [
    UserModule,
    PassportModule,
    JwtModule.register({
      secret: jwtSecret,
      signOptions: {expiresIn: "1d"}
  })],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService]
})
export class AuthModule {}
