import { Injectable } from '@nestjs/common';
import { UserService } from "../user/user.service";
import { JwtService } from "@nestjs/jwt";
import { User } from "../user/user.schema";
@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService
  ) {
  }

  async validateUser(username: string, password: string): Promise<User | null>{
      const user = await this.userService.findByUsername(username)

      if(!user || user.password != password){
        return null
      }
      return user
  }

  async login(user: User){
    const payload = {sub: user._id}

    return {
      access_token: this.jwtService.sign(payload)
    }
  }
}
