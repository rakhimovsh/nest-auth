import { Controller, Get, Post, Request, UseGuards } from "@nestjs/common";
import { LocalAuthGuard } from "./modules/auth/guards/local-auth.guard";
import { AuthService } from "./modules/auth/auth.service";
import { Roles } from "./security/role/roles.decorator";
import { Role } from "./security/role/role.enum";
import { RolesGuard } from "./security/role/roles.guard";
import { JwtAuthGuard } from "./modules/auth/guards/jwt-auth.guard";

@Controller()
export class AppController {
    constructor(private authService: AuthService) {
    }

    @UseGuards(LocalAuthGuard)
    @Post("auth/login")
    async login(@Request() req){
        return this.authService.login(req.user)
    }


    @UseGuards(JwtAuthGuard, RolesGuard)
    @Roles(Role.Admin)
    @Get('profile')
    getUser(@Request() req){
        return req.user
    }
}
